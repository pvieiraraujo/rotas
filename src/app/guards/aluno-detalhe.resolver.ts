import { AlunosService } from './../alunos/alunos.service';
import { Aluno } from './../alunos/aluno';
import { Observable } from 'rxjs';
import { ActivatedRouteSnapshot, RouterStateSnapshot, Resolve } from '@angular/router';
import { Injectable } from '@angular/core';


@Injectable()
export class AlunoDetalheResolver implements Resolve<Aluno> {

    constructor(private AlunosService: AlunosService) {}
    resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
        ): Observable<any>|Promise<any>|any {

            let id = route.params['id'];

            return this.AlunosService.getAluno(id);
    }
}