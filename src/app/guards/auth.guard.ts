import { FormCanDeactivated } from './form-candeactivated';
import { AuthService } from './../login/auth.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router, CanLoad, CanDeactivate } from '@angular/router';
import { Observable } from 'rxjs';
import { Route } from '@angular/compiler/src/core';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate, CanLoad, CanDeactivate<FormCanDeactivated> {

  constructor(private authService: AuthService,
    private Router: Router) { }
    
  usuarioEstaAutenticado(){}

  canActivate(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | boolean{

    if (this.authService.usuarioEstaAutenticado()){
      return true;
    }

    this.Router.navigate(['/login']);

    return this.verificarAcesso();
  }

  private verificarAcesso(){
    if (this.authService.usuarioEstaAutenticado()){
      return true;
    }
  }

  canActivateChild(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
): Observable<boolean>|Promise<boolean>|boolean {

    // if(state.url.includes('edit')){
    //     alert('Usuário sem acesso');
    //     return false;
    // }

    return true;
}

  	canLoad(route: Route
    ): Observable<boolean>|Promise<boolean>|boolean {
      
      return this.verificarAcesso();
    }

    canDeactivate(
      component: FormCanDeactivated,
      currentRoute: ActivatedRouteSnapshot, 
      currentState: RouterStateSnapshot
  ): Observable<boolean>|Promise<boolean>|boolean {


      return component.podeDesativar();
  }
    

}
