import { AlunosService } from './../alunos.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-aluno-detalhe',
  templateUrl: './aluno-detalhe.component.html',
  styleUrls: ['./aluno-detalhe.component.css']
})
export class AlunoDetalheComponent implements OnInit {

  aluno: any;
  inscricao: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private AlunosService: AlunosService,

  ) { }

  ngOnInit(): void {
    // this.inscricao = this.route.params.subscribe(
    //   (params: any) => {
    //     let id = params ['id'];

    //     this.aluno = this.AlunosService.getAluno(id);

    //   }
    // );

    this.inscricao = this.route.data.subscribe(
      (info) => {
        this.aluno = info.aluno;
      }
    );   

  }

  editarContato(){
    this.router.navigate(['/alunos', 'edit',this.aluno.id ]);
  }

  incluirContato(){
    this.router.navigate(['/alunos','novo']);
  }

  ngOnDestroy(){
    this.inscricao.unsubscribe();
  }

}
