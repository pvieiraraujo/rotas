import { FormCanDeactivated } from './../../guards/form-candeactivated';
import { AlunosService } from './../alunos.service';
import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-aluno-form',
  templateUrl: './aluno-form.component.html',
  styleUrls: ['./aluno-form.component.css']
})
export class AlunoFormComponent implements FormCanDeactivated {
  
  aluno: any;
  inscricao: Subscription;
  private formMudou: boolean = false;

  constructor(
    private route: ActivatedRoute,
    private AlunosService: AlunosService,

  ) { }

  ngOnInit(): void {
    this.inscricao = this.route.params.subscribe(
      (params: any) => {
        let id = params ['id'];

        this.aluno = this.AlunosService.getAluno(id);

        if(this.aluno === null){
          this.aluno = {};
        }

      }
    );
  }

  ngOnDestroy(){
    this.inscricao.unsubscribe();
  }

  onInput(){
    this.formMudou = true;
  }

  podeMudarRota(){

    if(this.formMudou){
      confirm('Tem certeza que deseja sair dessa página?')
    }

    return true;

  }

  podeDesativar(){
    return this.podeMudarRota();
  }

}
