import { AuthGuard } from './../guards/auth.guard';
import { AlunoDetalheResolver } from './../guards/aluno-detalhe.resolver';
import { AlunosService } from './alunos.service';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { AlunoDetalheComponent } from './aluno-detalhe/aluno-detalhe.component';
import { AlunoFormComponent } from './aluno-form/aluno-form.component';
import { AlunosComponent } from './alunos.component';
import { AlunosRoutingModule } from './alunos.routing.module';
import { FormsModule } from '@angular/forms';



@NgModule({
    imports:[
        CommonModule,
        AlunosRoutingModule,
        FormsModule
    ],
    exports:[],
    declarations:[
        AlunosComponent,
        AlunoFormComponent, 
        AlunoDetalheComponent
    ],
    providers:[
    AlunosService,
    AuthGuard,
    AlunoDetalheResolver,
    ],
})

export class AlunosModule { }