import { AlunoDetalheResolver } from './../guards/aluno-detalhe.resolver';
import { AlunosComponent } from './alunos.component';
import { NgModule } from '@angular/core';
import { RouterModule, CanDeactivate } from '@angular/router';

import { AlunoDetalheComponent } from './aluno-detalhe/aluno-detalhe.component';
import { AlunoFormComponent } from './aluno-form/aluno-form.component';


const alunosRoutes = [
    { path: '', component: AlunosComponent,
    children: [
        
        { path: 'novo', component: AlunoFormComponent},
        { path: ':id', component: AlunoDetalheComponent,
            resolve: { aluno : AlunoDetalheResolver}},
        { path: 'edit/:id', component: AlunoFormComponent},
            
    ]},

];

@NgModule({
    imports: [RouterModule.forChild(alunosRoutes)],
    exports: [RouterModule],

})

export class AlunosRoutingModule { }