import { AlunosService } from './alunos.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-alunos',
  templateUrl: './alunos.component.html',
  styleUrls: ['./alunos.component.css']
})
export class AlunosComponent implements OnInit {

  constructor(private AlunosService: AlunosService) { }

  alunos: any [] = [];

  ngOnInit(): void {
    this.alunos = this.AlunosService.getAlunos();
    this.alunos.forEach(aluno => {
      console.log(JSON.stringify(aluno));
    });
  }

}
